import Card from './components/Card.js';
import Data from './data.json';


function App(){
    // console.log(Data[3]);
    // for(let x=0;x<Data.length;x++)
    // {
    //     item.push(<Card titleText={Data[x].title} descText={Data[x].desc}/>);
    // }
    const users = [
        {name : "Mohana",
        age: 26,
        phone: [
                {
                home:"12345"
                },
                {
                office: "54321"
                }
            ]

        },
        {name : "Rishaov",
        age: 26,
        phone: [
                {
                home:"AAAAA"
                },
                {
                office: "BBBB"
                }
            ]
        }

    ]

  
    return (
    <div>
        <h1 className="headingStyle">My First React Project</h1>
        {Data.map((item, index) => <Card key={index} titleText={item.title} descText={item.desc}/>)}

        {users.map((user,index) => (
           <article key={index}>
            <h3>Name: {user.name}</h3>
            <p>Age: {user.age}</p>
            {
                user.phone.map((phone, index)=> 
                <div key={index}>
                    <p>{phone.home}</p>
                    <p>{phone.office}</p>
                </div>
                )
            }

           </article> 
        ))}
       
    </div>)
}

 export default App;